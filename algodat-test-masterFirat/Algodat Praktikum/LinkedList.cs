﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Algodat_Praktikum
{

    abstract class VerketteteListe :IEnumerable
    {
        List<int> Inhalt;
        protected Element first;
        public class Element
        {
            public Element next;
            public int data;

            public Element(int a)
            {
                data = a;
            }
        }
       public void Print()
        {

            Element tarzan = first;
            while (tarzan != null)
            {
                Console.WriteLine(tarzan.data);
                tarzan = tarzan.next;
            }
        }
        public IEnumerator GetEnumerator()
        {
            Element tarzan = first;
            while (tarzan != null)
            {
                yield return tarzan.data;
                tarzan = tarzan.next;
            }
        }
    }
    class SetSortedLinkedList : VerketteteListe, IDictonary
    {
        public void Sortiere()
        {
            Element tarzan = first;
            if (tarzan == null)
            {

            }
            else
            {
                while (tarzan.next != null)
                {
                    if (tarzan.data > tarzan.next.data)
                    {
                        int zv = tarzan.data;
                        tarzan.data = tarzan.next.data;
                        tarzan.next.data = zv;
                    }
                    tarzan = tarzan.next;
                }
            }
        }
         void IDictonary.print()
        {

            Element tarzan = first;
            while (tarzan != null)
            {
                Console.WriteLine(tarzan.data);
                tarzan = tarzan.next;
            }
        }
        public void Insert(int a)
        {
            Element tarzan = first;
            bool vorhanden = false;
            while (tarzan != null)
            {
                if (tarzan.data == a)
                {
                    vorhanden = true;
                    break;
                }
                tarzan = tarzan.next;
            }
            if (vorhanden == true)
            {

            }
            else
            {
                Element neu = new Element(a);
                if (first == null)
                {
                    first = neu;
                }
                else
                {
                    neu.next = first;
                    first = neu;
                }
                Sortiere();
            }
        }
        public void Delete(int elem)
        {
            if (first == null) // prüfen, ob Liste leer ist
            {
                return; // bricht somit ab
            }

            else
            {
                Element tarzan = first;

                if (tarzan.data == elem)
                {
                    first = tarzan.next;
                }
                while (tarzan.next != null)
                {
                    if (tarzan.next.data == elem)
                    {
                        if (tarzan.next.next != null)
                        {
                            tarzan.next = tarzan.next.next;
                        }
                        else
                        {
                            tarzan.next = null;
                        }
                    }
                }
            }
        }
        bool IDictonary.delete(int elem)
        {
            if (first == null) // prüfen, ob Liste leer ist
            {
                return false; // bricht somit ab
            }
            else
            {
                Delete(elem);
                return true;
            }
        }

        bool IDictonary.insert(int elem)
        {
     
            if (Search(elem))
            {
                return false;
            }
            else
            {
                Insert(elem);
                return true;
            }
        }
        public List<int> SetSortedLinked()
        {
            List<int> neu = new List<int>();
            foreach (int item in this)
            {
                neu.Add(item);
            }
            return neu;
        }
        public bool Search(int elem)
        {
            bool erg = false;

            foreach (int i in this)
            {
                if (elem == i)
                {
                    erg = true;
                }
            }
            return erg;
        }
        bool IDictonary.search(int elem)
        {
            return Search(elem);
        }
      
       public int[] SetSortedArray()
        {
            int[] neu = new int[9];
            int counter = 0;
            foreach (int item in this)
            {
                neu[counter] = item;
                counter++;
            }
            return neu;
        }
    }
    class SetUnsortedLinkedList : VerketteteListe,IDictonary

    {
        public void Insert(int a)
        {
            Element tarzan = first;
            bool vorhanden = false;
            while (tarzan != null)
            {
                if (tarzan.data == a)
                {
                    vorhanden = true;
                    break;
                }
                tarzan = tarzan.next;
            }
            if (vorhanden == true)
            {

            }
            else
            {
                Element neu = new Element(a);
                if (first == null)
                {
                    first = neu;
                }
                else
                {
                    neu.next = first;
                    first = neu;
                }
            }
        }
        void IDictonary.print()
        {

            Element tarzan = first;
            while (tarzan != null)
            {
                Console.WriteLine(tarzan.data);
                tarzan = tarzan.next;
            }
        }
        bool IDictonary.insert(int elem)
        {

            if (Search(elem))
            {
                return false;
            }
            else
            {
                Insert(elem);
                return true;
            }
        }
        public bool Search(int elem)
        {
            bool erg = false;

            foreach (int i in this)
            {
                if (elem == i)
                {
                    erg = true;
                }
            }
            return erg;
        }
        bool IDictonary.search(int elem)
        {
            return Search(elem);
        }
           public List<int> SetUnsortedLinked()
        {
            List<int> neu = new List<int>();
            MultiSetUnsortedLinkedList test = new MultiSetUnsortedLinkedList();
            foreach (int item in this)
            {
                test.Insert(item);
            }
            foreach (int i in test)
            {
                neu.Add(i);
            }
            return neu;
        }

        public void Delete(int elem)
        {
            if (first == null) // prüfen, ob Liste leer ist
            {
                return; // bricht somit ab
            }

            else
            {
                Element tarzan = first;

                if (tarzan.data == elem)
                {
                    first = tarzan.next;
                }
                while (tarzan.next != null)
                {
                    if (tarzan.next.data == elem)
                    {
                        if (tarzan.next.next != null)
                        {
                            tarzan.next = tarzan.next.next;
                        }
                        else
                        {
                            tarzan.next = null;
                        }
                    }
                }
            }
        }
        bool IDictonary.delete(int elem)
        {
            if (first == null) // prüfen, ob Liste leer ist
            {
                return false; // bricht somit ab
            }
            else
            {
                Delete(elem);
                return true;
            }
        }
        public int[] SetUnsortedArray()
        {
            MultiSetUnsortedLinkedList test = new MultiSetUnsortedLinkedList();
            int[] neu = new int[9];
            int counter = 0;

            foreach (int item in this)
            {
                test.Insert(item);
            }
            foreach (int i in neu)
            {
                neu[counter] = i;
                counter++;
            }
            return neu;
        }
       

    }
    class MultiSetSortedLinkedList : VerketteteListe,IDictonary,IEnumerable
    {
        public void Delete(int elem)
        {
            if (first == null) // prüfen, ob Liste leer ist
            {
                return; // bricht somit ab
            }

            else
            {
                Element tarzan = first;

                if (tarzan.data == elem)
                {
                    first = tarzan.next;
                }
                while (tarzan.next != null)
                {
                    if (tarzan.next.data == elem)
                    {
                        if (tarzan.next.next != null)
                        {
                            tarzan.next = tarzan.next.next;
                        }
                        else
                        {
                            tarzan.next = null;
                        }
                    }
                }
            }
        }
        public bool Search(int elem)
        {
            bool erg = false;

            foreach (int i in this)
            {
                if (elem == i)
                {
                    erg = true;
                }
            }
            return erg;
        }
        bool IDictonary.search(int elem)
        {
            return Search(elem);
        }
        void IDictonary.print()
        {

            Element tarzan = first;
            while (tarzan != null)
            {
                Console.WriteLine(tarzan.data);
                tarzan = tarzan.next;
            }
        }
        bool IDictonary.delete(int elem)
        {
            if (first == null) // prüfen, ob Liste leer ist
            {
                return false; // bricht somit ab
            }
            else
            {
                Delete(elem);
                return true;
            }
        }

        public int[] MultiSetSortedArray()
        {
            int[] neu = new int[9];
            int counter = 0;
            foreach (int item in this)
            {
                neu[counter] = item;
                counter++;
            }
            return neu;
        }
        public List<int> MultiSetSortedList()
        {
            List<int> SortedList = new List<int>();
            foreach (int item in this)
            {
                SortedList.Add(item);
            }
            return SortedList;

        }
        public int[] SetSortedArray()
        {
            int[] neu = new int[9];
            int counter = 0;
            foreach (int item in this)
            {
                neu[counter] = item;
                counter++;
            }
            return neu;
        }
      public List<int> SetSortedLinkedList()
        {
            List<int> neu = new List<int>();
            foreach (int item in this)
            {
                neu.Add(item);
            }
            return neu;
        }


        public void Insert(int a)
        {
            Element neu = new Element(a);
            if (first == null)
            {
                first = neu;
            }
            else
            {
                neu.next = first;
                first = neu;
            }
            Sortiere();
        }
        bool IDictonary.insert(int elem)
        {
            if (Search(elem))
            {
                return false;
            }
            else
            {
                Insert(elem);
                return true;
            }
        }
      

       public void Sortiere()
        {
            Element tarzan = first;
            if (tarzan == null)
            {

            }
            else
            {
                while(tarzan.next!=null)
                {
                    if (tarzan.data > tarzan.next.data)
                    {
                        int zv = tarzan.data;
                        tarzan.data = tarzan.next.data;
                        tarzan.next.data = zv;
                    }
                    tarzan = tarzan.next;
                }
            }
        }
    }
    class MultiSetUnsortedLinkedList : VerketteteListe, IDictonary
    {
        void AddSetSorted(int a)
        {                                                                           //AddSetSorted ist eine hilfsmethode;
            Element tarzan = first;
            bool vorhanden = false;
            while (tarzan != null)
            {
                if (tarzan.data == a)
                {
                    vorhanden = true;
                    break;
                }
                tarzan = tarzan.next;
            }
            if (vorhanden == true)
            {

            }
            else
            {
                Element neu = new Element(a);
                if (first == null)
                {
                    first = neu;
                }
                else
                {
                    neu.next = first;
                    first = neu;
                }
            }
        }
        public void Delete(int elem)
        {
            if (first == null) // prüfen, ob Liste leer ist
            {
                return; // bricht somit ab
            }

            else
            {
                Element tarzan = first;

                if (tarzan.data == elem)
                {
                    first = tarzan.next;
                }
                while (tarzan.next != null)
                {
                    if (tarzan.next.data == elem)
                    {
                        if (tarzan.next.next != null)
                        {
                            tarzan.next = tarzan.next.next;
                        }
                        else
                        {
                            tarzan.next = null;
                        }
                    }
                }
            }
        }
        bool IDictonary.delete(int elem)
        {
            if (first == null) // prüfen, ob Liste leer ist
            {
                return false; // bricht somit ab
            }
            else
            {
                Delete(elem);
                return true;
            }
        }   
        public bool Search(int elem)
        {
            bool erg = false;

            foreach (int i in this)
            {
                if (elem == i)
                {
                    erg = true;
                }
            }
            return erg;
        }
        bool IDictonary.search(int elem)
        {
            return Search(elem);
        }

        void IDictonary.print()
        {

            Element tarzan = first;
            while (tarzan != null)
            {
                Console.WriteLine(tarzan.data);
                tarzan = tarzan.next;
            }
        }

        public void Insert(int a)
        {
            Element neu = new Element(a);
            if (first == null)
            {
                first = neu;
            }
            else
            {
                neu.next = first;
                first = neu;
            }
        }
        bool IDictonary.insert(int elem)
        {
            if (Search(elem))
            {
                return false;
            }
            else
            {
                Insert(elem);
                return true;
            }
        }


        public int[] MultiSetUnsortedArray()
        {
            int[] neu = new int[9];
            int counter = 0;
            foreach (int item in this)
            {
                neu[counter] = item;
                counter++;
            }
            return neu;
        }
     

        public List<int> MultiSetUnsortedList()
        {
            List<int> SortedList = new List<int>();
            foreach (int item in this)
            {
                SortedList.Add(item);
            }
            return SortedList;
        }

        public int[] SetUnsortedArray()
        {

            MultiSetUnsortedLinkedList test = new MultiSetUnsortedLinkedList();
            int[] neu = new int[9];
            int counter = 0;

            foreach (int item in this)
            {
                test.AddSetSorted(item);
            }
            foreach (int i in neu)
            {
                neu[counter] = i;
                counter++;
            }
            return neu;
        }

        public List<int> SetUnsortedLinkedList()
        {
            List<int> neu = new List<int>();
            MultiSetUnsortedLinkedList test = new MultiSetUnsortedLinkedList();
            foreach (int item in this)
            {
                test.AddSetSorted(item);
            }
            foreach (int i in test)
            {
                neu.Add(i);
            }
            return neu;
        }
    }
    
}
