﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Algorithmen Datenstrukturen Praktikum";
            ConsoleKeyInfo ckey;
            Console.WriteLine("Wählen Sie einen der Dictonary Datentypen:");
            Console.WriteLine("\t[1] -- drücken für BinSearchTree");
            Console.WriteLine("\t[2] -- drücken für AVLTree");
            Console.WriteLine("\t[3] -- drücken für Array");
            Console.WriteLine("\t[4] -- drücken für Verkettete Liste");
            ckey = Console.ReadKey();
            switch (ckey.Key)
            {
                case ConsoleKey.D1:
                    {
                        BinSearchTree binsearchtree = new BinSearchTree();
                        Console.WriteLine("\nBinSearchTree");
                        while (ckey.Key != ConsoleKey.Escape)
                        {
                            Console.WriteLine("(1)Insert ; (2)Delete ; (3)Search ; (4)Print");
                            ConsoleKeyInfo ck;
                            ck = Console.ReadKey();
                            if (ck.Key == ConsoleKey.D1)
                            {
                                Console.WriteLine("Welche Zahl möchten Sie einfügen?");
                                int insertZahl = Convert.ToInt16(Console.ReadLine());
                                binsearchtree.Insert(insertZahl);
                            }
                            if (ck.Key == ConsoleKey.D2)
                            {
                                Console.WriteLine("Welche Zahl möchten Sie löschen?");
                                int deleteZahl = Convert.ToInt16(Console.ReadLine());
                                binsearchtree.Delete(deleteZahl);
                            }
                            if (ck.Key == ConsoleKey.D3)
                            {
                                Console.WriteLine("Welche Zahl möchten Sie suchen?");
                                int searchZahl = Convert.ToInt16(Console.ReadLine());
                                Console.WriteLine(binsearchtree.Search(searchZahl));
                            }
                            if (ck.Key == ConsoleKey.D4)
                            {
                                binsearchtree.Print();
                            }
                        }

                        break;

                    }
                case ConsoleKey.D2:
                    {
                        AVLTree avltree = new AVLTree();
                        Console.WriteLine("\nAVLTree");
                        while (ckey.Key != ConsoleKey.Escape)
                        {
                            Console.WriteLine("(1)Insert ; (2)Delete ; (3)Search ; (4)Print");
                            ConsoleKeyInfo ck;
                            ck = Console.ReadKey();
                            if (ck.Key == ConsoleKey.D1)
                            {
                                Console.WriteLine("Welche Zahl möchten Sie einfügen?");
                                //int insertZahl = Convert.ToInt16(Console.ReadLine());
                                //avltree.Add(insertZahl);
                                avltree.Add(2);
                                avltree.Add(12);
                                avltree.Add(16);
                                avltree.Add(17);
                                avltree.Add(34);
                                avltree.Add(37);
                                avltree.Add(41);
                                avltree.Add(50);
                                avltree.Add(54);
                                avltree.Add(55);
                                avltree.Add(63);
                                avltree.Add(69);
                                avltree.Add(71);

                            }
                            if (ck.Key == ConsoleKey.D2)
                            {
                                Console.WriteLine("Welche Zahl möchten Sie löschen?");
                                int delteZahl = Convert.ToInt16(Console.ReadLine());
                                avltree.Delete(delteZahl);
                            }
                            if (ck.Key == ConsoleKey.D3)
                            {
                                Console.WriteLine("Welche Zahl möchten Sie suchen?");
                                int searchZahl = Convert.ToInt16(Console.ReadLine());
                                avltree.Find(searchZahl);
                            }
                            if (ck.Key == ConsoleKey.D4)
                            {
                                avltree.PrintAVL();
                            }
                        }
                        break;
                    }
                case ConsoleKey.NumPad3:
                    {
                        Console.WriteLine("Array");
                        break;
                    }
                case ConsoleKey.NumPad4:
                    {
                        Console.WriteLine("Verkettete Liste");
                        break;
                    }
                    Console.ReadLine();
            }

        }
    }
}
