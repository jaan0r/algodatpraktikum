﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    class BinSearchTree : ISetSorted,IDictonary
    {
        #region Consturctor
        public BinSearchTree()
        {
            root = null;
        }
        #endregion
        
        #region Properties
        BSNode root;


        int levelcounter = 1;
        public BSNode Root
        {
            get { return root; }
            set { root = value; }
        }


        public int Levelcounter
        {
            get { return levelcounter; }
            set { levelcounter = value; }
        }
        #endregion

        public void BSTDelete(BSNode node, int Value)
        {
            // Zunächst wird geprüft ob wir im Baum nach links oder nac rechts absteigen müssen
            if (Value < node.Element)
            {
                // Rufe rekursiv die Löschenfunktion auf, da der gesuchte Wert(Value) kleiner ist als der Wert der aktuellen Node -> führe die Suche Links weiter
                BSTDelete(node.Left, Value);
            }
            else if (Value > node.Element)
            {
                // Analog zu Value < node.Value, nur dass man hier nach rechts weitergeht
                BSTDelete(node.Right, Value);
            }
            else // <- Das bedeutet dass wir unsere Node gefunden haben
            {
                if (node.Left != node.Right) // Die gefundene Node hat sowohl Left als auch Right
                {
                    BSNode min = findMin(node.Right); // Finde zunächst den nächsten kleinsten Wert der größer ist als der Wert der aktuellen Node
                    node.Element = min.Element; // Schreibe den Wert von min in die Aktuelle Node
                    BSTDelete(min, min.Element); // Rekursives Löschen der Min-Node (Die ja vielleicht auch Children hat)
                }
                else if (node.Left != null) // Hat nur Left
                {
                    replaceNode(node, node.Left); // ersetze die aktuelle node mit den Left Child
                }
                else if (node.Right != null) // Hat nur Right
                {
                    replaceNode(node, node.Right); // ersetze die aktuelle node mit den Right Child
                }
                else// Keine Children
                {
                    replaceNode(node, null); // ersetze die aktuelle node mit Null -> node wird einfach gelöscht
                }
            }
        }

        public void replaceNode(BSNode node, BSNode newNode)
        {
            // Hat die Node einen Parent ?
            if (node.Previous != null)
            {
                // Ist die aktuelle Node das Left Child?
                if (node == node.Previous.Left)
                {
                    // Setze das Left Child des Parents auf den neuen Wert
                    node.Previous.Left = newNode;
                }
                else // Ist Right Child
                {
                    // Setze das Right Child des Parents auf den neuen Wert
                    node.Previous.Right = newNode;
                }
            }
            // Neue Node gesetzt?, also nicht Null
            if (newNode != null)
            {
                // Setze den Parent der neuen node auf den Parent der ersetzten Node
                newNode.Previous = node.Previous;
            }
        }

        public BSNode findMin(BSNode node)
        {
            BSNode curr = node;
            while (curr.Left != null)
            {
                curr = curr.Left;
            }
            return curr;
        }
        
        public void InsertBS(BSNode bsNode, int value)
        {
            if (root == null)
            //empty Tree
            {
                BSNode node = new BSNode(value);
                this.root = node;
            }
            //Left
            else if (value < bsNode.Element)
            {
                if (bsNode.Left != null)
                {
                    levelcounter++;
                    InsertBS(bsNode.Left, value);
                }
                else
                {
                    bsNode.Left = new BSNode(value);
                    bsNode.Left.Level = levelcounter;
                    bsNode.Left.Previous = bsNode;
                    Levelcounter = 1;
                }
            }
            //right
            else if (value > bsNode.Element)
            {
                if (bsNode.Right != null)
                {
                    levelcounter++;
                    InsertBS(bsNode.Right, value);
                }
                else
                {
                    bsNode.Right = new BSNode(value);
                    bsNode.Right.Level = levelcounter;
                    bsNode.Right.Previous = bsNode;
                    Levelcounter = 1;

                }
            }

        }

        public bool _Search(BSNode bsNode, int value)
        {
            if (root == null)
                return false;
            else if (bsNode == null)
            {
                return false;
            }
            else if (value < bsNode.Element)
            {
                return _Search(bsNode.Left, value);
            }
            else if (value > bsNode.Element)
            {
                return _Search(bsNode.Right, value);
            }
            return true;
        }

        #region Hilfsmethoden für Ausgabe

        bool IDictonary.search(int elem)
        {
            return _Search(Root, elem);
        }

        bool IDictonary.delete(int elem)
        {
            if (_Search(Root, elem))
            {
                BSTDelete(Root, elem);
                return true;
            }
            else
                return false;
        }

        bool IDictonary.insert(int elem)
        {
            if (_Search(Root, elem))
            {
                return false;
            }
            else
            {
                InsertBS(root, elem);
                return true;
            }
        }
       void IDictonary.print()
        {
            if (root != null)
                root.Print(root);
        }
        #endregion
    }
}






