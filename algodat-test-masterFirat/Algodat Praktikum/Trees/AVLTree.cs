﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    class AVLTree : BinSearchTree, IDictonary
    {
        
        public AVLNode root;
        new public AVLNode Root
        {
            get { return root; }
            set { root = value; }
        }

        public AVLTree()
        {
        }
        public void Add(int data)
        {
            AVLNode newItem = new AVLNode(data);
            if (root == null)
            {
                root = newItem;
            }
            else
            {
                root = RecursiveInsert(root, newItem);
            }
        }

        private AVLNode RecursiveInsert(AVLNode current, AVLNode n)
        {
            if (current == null)
            {
                current = n;
                return current;
            }
            else if (n.Element < current.Element)
            {   
                current.left = RecursiveInsert(current.Left, n);
                current = balance_tree(current);              
            }
            else if (n.Element > current.Element)
            {
                current.right = RecursiveInsert(current.Right, n);
                current = balance_tree(current);               
            }
            return current;
        }

        private AVLNode balance_tree(AVLNode current)
        {
            int b_factor = balance_factor(current);
            if (b_factor > 1)
            {
                if (balance_factor(current.Left) > 0)
                {
                    current = RotateLL(current);
                }
                else
                {
                    current = RotateLR(current);
                }
            }
            else if (b_factor < -1)
            {
                if (balance_factor(current.Right) > 0)
                {
                    current = RotateRL(current);
                }
                else
                {
                    current = RotateRR(current);
                }
            }
            return current;
        }

        private AVLNode Delete(AVLNode current, int target)
        {
            AVLNode parent;
            if (current == null)
            { return null; }
            else
            {
                //left subtree
                if (target < current.Element)
                {
                    current.left = Delete(current.Left, target);
                    if (balance_factor(current) == -2)
                    {
                        if (balance_factor(current.Right) <= 0)
                        {
                            current = RotateRR(current);
                        }
                        else
                        {
                            current = RotateRL(current);
                        }
                    }
                }
                //right subtree
                else if (target > current.Element)
                {
                    current.right = Delete(current.Right, target);
                    if (balance_factor(current) == 2)
                    {
                        if (balance_factor(current.Left) >= 0)
                        {
                            current = RotateLL(current);
                        }
                        else
                        {
                            current = RotateLR(current);
                        }
                    }
                }
                //if target is found
                else
                {
                    if (current.right != null)
                    {
                        //delete its inorder successor
                        parent = current.Right;
                        while (parent.left != null)
                        {
                            parent = parent.Left;
                        }
                        current.Element = parent.Element;
                        current.right = Delete(current.Right, parent.Element);
                        if (balance_factor(current) == 2)   //rebalancing
                        {
                            if (balance_factor(current.Left) >= 0)
                            {
                                current = RotateLL(current);
                            }
                            else { current = RotateLR(current); }
                        }
                    }
                    else
                    {   //if current.left != null
                        return current.Left;
                    }
                }
            }
            return current;
        }

        public void Find(int key)
        {
            if (Find(key, Root).Element == key)
            {
                Console.WriteLine("{0} was found!", key);
            }
            else
            {
                Console.WriteLine("Nothing found!");
            }
        }

        private AVLNode Find(int target, AVLNode current)
        {

            if (target < current.Element)
            {
                if (target == current.Element)
                {
                    return current;
                }
                else
                    return Find(target, current.Left);
            }
            else
            {
                if (target == current.Element)
                {
                    return current;
                }
                else
                    return Find(target, current.Right);
            }
        }

        public void DisplayTree()
        {
            if (root == null)
            {
                Console.WriteLine("Tree is empty");
                return;
            }
            InOrderDisplayTree(root);
            Console.WriteLine();
        }

        private void InOrderDisplayTree(AVLNode current)
        {
            if (current != null)
            {
                InOrderDisplayTree(current.Left);
                Console.Write("({0}) ", current.Element);
                InOrderDisplayTree(current.Right);
            }
        }

        private int max(int l, int r)
        {
            return l > r ? l : r;
        }

        private int getHeight(AVLNode current)
        {
            int height = 0;
            if (current != null)
            {
                int l = getHeight(current.Left);
                int r = getHeight(current.Right);
                int m = max(l, r);
                height = m + 1;
            }
            return height;
        }

        private int balance_factor(AVLNode current)
        {
            int l = getHeight(current.Left);
            int r = getHeight(current.Right);
            int b_factor = l - r;
            return b_factor;
        }

        #region Rotations
        private AVLNode RotateRR(AVLNode parent)
        {
            AVLNode pivot = parent.Right;
            parent.right = pivot.left;
            pivot.left = parent;
            return pivot;
        }
        private AVLNode RotateLL(AVLNode parent)
        {
            AVLNode pivot = parent.Left;
            parent.left = pivot.right;
            pivot.right = parent;
            return pivot;
        }
        private AVLNode RotateLR(AVLNode parent)
        {
            AVLNode pivot = parent.Left;
            parent.left = RotateRR(pivot);
            return RotateLL(parent);
        }
        private AVLNode RotateRL(AVLNode parent)
        {
            AVLNode pivot = parent.Right;
            parent.right = RotateLL(pivot);
            return RotateRR(parent);
        }
        #endregion

        #region Help Methods
        void IDictonary.delete(int elem)
        {
            Root = Delete(Root, elem);
        }

        #endregion 
    }
}
