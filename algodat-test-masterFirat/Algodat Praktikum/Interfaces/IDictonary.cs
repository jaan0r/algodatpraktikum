﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    interface IDictonary : IMultiSet , IMultiSetSorted // test
    {
        bool search(int elem);
        bool insert(int elem);
        bool delete(int elem);
        void print();
    }
}
