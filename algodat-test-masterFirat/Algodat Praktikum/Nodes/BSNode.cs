﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    public class BSNode 
    {
        public BSNode left;
        public BSNode right;
        public BSNode previous;
        public int element;
        public int level = 0;

        #region Constructor
        public BSNode(int Element=0)
        {
            element = Element;
        }
        #endregion

        #region Properties
        public int Level
        {
            get { return level; }
            set { level = value; }
        }
        public BSNode Previous
        {
            get { return previous; }
            set { previous = value; }
        }
        public BSNode Left
        {
            get { return left; }
            set { left = value; }
        }

        public BSNode Right
        {
            get { return right; }
            set { right = value; }
        }

        public int Element
        {
            get { return element; }
            set { element = value; }
        }

        #endregion

        public void Print(BSNode n) 
        {
            if (n.right != null)
            {
                Print(n.right);
            }
            for (int i = 0; i < n.level; i++)
            {
                Console.Write("-");
            }
            Console.Write(" " + n.element);
            Console.WriteLine("");
            Console.WriteLine("");
            if (n.left != null)
            {
                Print(n.left);
            }
        }
    }

}
