﻿using System;
namespace Algodat_Praktikum
{
    public class AVLNode : BSNode
    {
        #region Instances    
        public int height;
        new public AVLNode left;
        new public AVLNode right;
        new public int element;
        new public int level = 0;
        #endregion

        #region Contructor
        public AVLNode(int ele)
        {
            element = ele;
        }
        #endregion

        #region Properties
        public int LevelAVL
        {
            get { return level; }
            set { level = value; }
        }
        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        new public AVLNode Left
         {
             get { return left; }
             set { left = value; }
         }
        new public AVLNode Right
        {
            get { return right; }
            set { right = value; }
        }
        new public int Element
        {
            get { return element; }
            set { element = value; }
        }
        #endregion 

        //public void PrintAVL(AVLNode n)
        //{
        //    if (n.right != null)
        //    {
        //        Print(n.right);
        //    }
        //    for (int i = 0; i < n.level; i++)
        //    {
        //        Console.Write("-");
        //    }
        //    Console.Write(" " + n.element);
        //    Console.WriteLine("");
        //    Console.WriteLine("");
        //    if (n.left != null)
        //    {
        //        PrintAVL(n.Left);
        //    }
        //}
    }


}
