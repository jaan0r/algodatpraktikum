﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    class Hash
    {
        static int m = 0;
        public int?[] hashtabelle = new int?[m];

        public int M
        {
            get { return m; }
            set { m = value; }
        }
        public bool Search(int x)
        {
            for (int i = 0; i < hashtabelle.Length; i++)
            {
                if (hashtabelle[i] == x)
                    return true;
            }
            return false;
        }
        public void Print()
        {
            for (int i = 0; i < hashtabelle.Length; i++)
            {
                Console.WriteLine(hashtabelle[i] + "pos : " + i);
            }
        }


    }
    class HashTabSepChain : Hash
    {
        public HashTabSepChain(int _m)
        {
            M = _m;
        }
        public bool Insert(int x)
        {
            return false;
        }

    }
    class HashTabQuadProb : Hash
    {


        public HashTabQuadProb(int _m)
        {
            M = _m;
        }

        public bool Insert(int s)
        {
            bool freeslot = false;
            int j = 0;
            int hash = s % M;
            int arraypos1;
            int arraypos2;

            while (!freeslot)
            {
                arraypos1 = (hash + j * j) % M;
                arraypos2 = (hash - j * j) % M;
                if (!hashtabelle[arraypos1].HasValue)
                {
                    hashtabelle[arraypos1] = s;
                    freeslot = true;
                }
                else if (!hashtabelle[arraypos2].HasValue)
                {
                    hashtabelle[arraypos2] = s;
                    freeslot = true;
                }
                j++;


            }

            return freeslot;
        }
    }
}
